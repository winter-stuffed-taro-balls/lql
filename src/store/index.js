import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

// import vuexPersist from "vuex-persist";
// export default new Vuex.Store({
//   state: {},
//   mutations: {},
//   actions: {},
//   modules: {},
//   plugins: [
//     new vuexPersist({
//       storage: window.localStorage,
//     }).plugin,
//   ],
// });
//vuex下的持久化插件 用在vuex里的插件  ！！！所以只能在vuex/store/index 引入
import vuexPersist from "vuex-persist";//第二步 引入 本地存储持久化插件  第一步安装cnpm i vuex-persist --save

export default new Vuex.Store({//Store仓库的意思
  state: {//存数据  状态的意思  status也是状态的意思          

  },
  mutations: {//相当于methods 只有mutations才可以修改该state  通过this.$state.commit(名,参数)传递 转变的意思 commit//委托的意思 第一参数state

  },
  actions: {//动作的意思 执行异步操作的 dispatch//派遣的意思 actions第一个参数永远是context

  },
  modules: {//加载模块的意思 当数据state较多的时候可以分模块管理

  },
  getters: {//相当于计算属性 获取的人的意思

  },
  plugins: [//第三步配置全局
    new vuexPersist({
      storage: window.localStorage,
    }).plugin,
  ],
});