import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home'
Vue.use(VueRouter)

const routes = [

  {
    path: '/',
    name: 'Home',
    component: Home
  },
    {
        path: '/process',
        name:'Process',
        component:()=>import('../views/Process')
    },

  {
    path: '/devsjs',
    name: 'DevSjs',
    component: () => import( '../views/DevSjs.vue')
  },
  {
    path: '/gonglue',
    name: 'Gonglue',
    
    component: () => import('../views/Gonglue.vue')
  },
  {
    path: '/zhuangxiuzhibo',
    name: 'Zhuangxiuzhibo',
    
    component: () => import('../views/Zhuangxiuzhibo.vue')
  },
  {
    path: '/xiaoguo',
    name: 'xiaoguo',
    
    component: () => import('../views/xiaoguo.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router
