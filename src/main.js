import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

Vue.config.productionTip = false;

import ElementUI, { Link } from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
Vue.use(ElementUI);
//引入全部的element-ui组件

import axios from "axios";
console.log(process.env.VUE_APP_BASE_URL);
Vue.prototype.$axios = axios;

// axios.defaults.baseURL= process.env. VUE_APP_BASE_URL

import Vant from "vant";
import "vant/lib/index.css";
Vue.use(Vant);
//全局引入vant

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
