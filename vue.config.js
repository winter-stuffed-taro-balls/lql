module.exports={
    devServer:{
        publicPath:'./',
        open:true,
        port:8080,
        proxy:{
            '/api':{
                target:'http://m.sirfang.com/api',
                changeOrigin:true,
                pathRewrite:{
                    '^/api':''
                }
            }
        }

    }
}